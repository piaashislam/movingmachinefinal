
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { RouterModule, Routes } from '../../node_modules/@angular/router';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
// import { FormsModule } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './Components/navbar/navbar.component';
import { HomePageComponent } from './Components/home-page/home-page.component';
import { CarousalBannerComponent } from './Components/carousal-banner/carousal-banner.component';
import { FooterComponent } from './Components/footer/footer.component';
import { CategoryPageComponent } from './Components/category-page/category-page.component';
import { ProducDetailsPageComponent } from './Components/produc-details-page/produc-details-page.component';
import { CategoryByBrandPageComponent } from './Components/category-by-brand-page/category-by-brand-page.component';
import { HomeComponent } from './Components/home/home.component';
import { CategoriesComponent } from './Components/categories/categories.component';
import { ContactPageComponent } from './Components/contact-page/contact-page.component';
import { ProductsByCatPageComponent } from './Components/products-by-cat-page/products-by-cat-page.component';
import { PaginationComponent } from './Components/pagination/pagination.component';
import { AboutPageComponent } from './Components/about-page/about-page.component';
import { ProductEnquiryPageComponent } from './Components/product-enquiry-page/product-enquiry-page.component';




const coreRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: HomePageComponent
      },
      {
        path: 'equipment/categories',
        pathMatch: 'full',
        component: ProductsByCatPageComponent
      },
      {
        path: 'equipment/categories/brands',
        pathMatch: 'full',
        component: CategoryByBrandPageComponent
      },
      {
        path: 'equipment/categories/product-details',
        pathMatch: 'full',
        component: ProducDetailsPageComponent
      },
      {
        path: 'contact',
        pathMatch: 'full',
        component: ContactPageComponent
      },
      {
        path: 'about',
        pathMatch: 'full',
        component: AboutPageComponent
      },
      {
        path: 'contact',
        pathMatch: 'full',
        component: ContactPageComponent
      },
      {
        path: 'product-enquiry',
        pathMatch: 'full',
        component: ProductEnquiryPageComponent
      }

      // {
      //   path: 'dashboard',
      //   component: DashboardComponent
      // }
    ]
  }
];


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomePageComponent,
    CarousalBannerComponent,
    FooterComponent,
    CategoryPageComponent,
    ProducDetailsPageComponent,
    CategoryByBrandPageComponent,
    HomeComponent,
    CategoriesComponent,
    ContactPageComponent,
    ProductsByCatPageComponent,
    PaginationComponent,
    AboutPageComponent,
    ProductEnquiryPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    RouterModule.forRoot(coreRoutes),
    // FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
