import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsByCatPageComponent } from './products-by-cat-page.component';

describe('ProductsByCatPageComponent', () => {
  let component: ProductsByCatPageComponent;
  let fixture: ComponentFixture<ProductsByCatPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsByCatPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsByCatPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
